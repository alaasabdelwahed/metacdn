'use strict';

angular.module('mediaApp',[])
	.constant('baseURL','http://localhost:8081/media-app/mediaservice')
        .service('getMediaService', ['$http', 'baseURL',function($http, baseURL) {
        	this.getMedias = function(){
        		return $http.get(baseURL);
        	};
        }])
        
        .service('sortMediaService', ['$http', 'baseURL',function($http, baseURL) {
        	this.sortByTitle = function(ascendingTitle){
        		if(ascendingTitle){
        			return $http.get(baseURL+"?sort=title")
        		}
        		else{
        			return $http.get(baseURL+"?sort=-title")
        		};
        	};
        	
        	this.sortByFileSize = function(ascendingSize){
        		if(ascendingSize){
        			return $http.get(baseURL+"?sort=fileSize")
        		}
        		else{
        			return $http.get(baseURL+"?sort=-fileSize")
        		};
        	};
        	
        	this.sortByCreatedTime = function(ascendingTime){
        		if(ascendingTime){
        			return $http.get(baseURL+"?sort=createdTime")
        		}
        		else{
        			return $http.get(baseURL+"?sort=-createdTime")
        		};
        	};
        }]);
        