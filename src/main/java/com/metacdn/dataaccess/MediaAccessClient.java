/**
 * This class is responsible for connecting with the API to retrieve the JSON data.
 */
package com.metacdn.dataaccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.DatatypeConverter;

/**
 * @author Alaa Abdelwahed
 */
public class MediaAccessClient {
	
	private final String CONFIG_FILE_NAME = "config.properties";
	private final String API_URL = "apiURL";
	private final String USERNAME = "username";
	private final String PASSWORD = "password";
	private URL url;
	private String username;
	private String password;
	
	public MediaAccessClient() throws IOException{
		setConfigProperties();
	}
	
	private void setConfigProperties() throws IOException{
		//Set the url, name & password by reading configured
		//properties from the config file
		Properties propFile = new Properties();
		InputStream inputStream = getClass().getClassLoader().
				getResourceAsStream(CONFIG_FILE_NAME);
		
		if (inputStream != null){
			propFile.load(inputStream);
			setUsername(propFile.getProperty(USERNAME));
			setPassword(propFile.getProperty(PASSWORD));
			setUrl(propFile.getProperty(API_URL));
		}
	}
	
	public String getJSONData() throws IOException{
		//connect to the REST API and retrieve the JSON data
		BufferedReader reader;
		StringBuilder builder = new StringBuilder();
		HttpsURLConnection connection = (HttpsURLConnection)
				getUrl().openConnection();
		
		try{
			String userCredentails = getUsername() + ":" + getPassword();
			String basicAuthentication = "Basic " + 
					DatatypeConverter.printBase64Binary(userCredentails.getBytes());
	
			connection.setRequestProperty ("Authorization", basicAuthentication);
			if (connection.getResponseCode() != 200) {
				throw new IOException(connection.getResponseMessage());
			}

			reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
			  builder.append(line);
			}
			reader.close();
		}catch(IOException io){
			throw new IOException(io.getMessage());
		}
		finally{
			connection.disconnect();
		}
		return builder.toString();
	}
	
	private URL getUrl() {
		return url;
	}

	private void setUrl(String urlString) throws MalformedURLException {
		this.url = new URL(urlString);
	}

	private String getUsername() {
		return username;
	}

	private void setUsername(String username) {
		this.username = username;
	}

	private String getPassword() {
		return password;
	}

	private void setPassword(String password) {
		this.password = password;
	}
}
