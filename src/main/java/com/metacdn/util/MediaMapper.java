/**
 * This class converts the JSON data to a POJO and vice versa
 */
package com.metacdn.util;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.metacdn.domain.MediaContainer;

/**
 * @author Alaa Abdelwahed
 *
 */
public class MediaMapper {
	
	public MediaContainer convertJSONtoObject(String jsonString) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		MediaContainer mediaCon = new MediaContainer();
		
		mediaCon = mapper.readValue(jsonString, MediaContainer.class);
		
		return mediaCon;
	}
	
	
	public String convertObjectToJSON(Object obj) throws JsonParseException, JsonMappingException, IOException{
	
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(obj);
		
		return json;
	}
	
	
	
	
}
