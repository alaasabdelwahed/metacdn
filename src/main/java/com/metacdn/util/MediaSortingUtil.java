package com.metacdn.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.metacdn.domain.Media;

public class MediaSortingUtil {

	public List<Media> sortList(List<Media> mediaList, String sort) {
		boolean ascending = true;

		if (sort.startsWith("-")) {
			ascending = false;
			sort = sort.substring(1);
		}

		if (("title").equalsIgnoreCase(sort)) {
			mediaList = this.sortByTitle(mediaList, ascending);
		} else if ("fileSize".equalsIgnoreCase(sort)) {
			mediaList = this.sortByFileSize(mediaList, ascending);
		} else if ("createdTime".equalsIgnoreCase(sort)) {
			mediaList = this.sortByCreatedTime(mediaList, ascending);
		}

		return mediaList;
	}

	public List<Media> sortByTitle(List<Media> mediaList, boolean ascending) {

		Collections.sort(mediaList, this.titleComparator);

		if (ascending == false) {
			Collections.reverse(mediaList);
		}
		return mediaList;
	}

	public List<Media> sortByFileSize(List<Media> mediaList, boolean ascending) {

		Collections.sort(mediaList, this.fileSizeComparator);

		if (ascending == false) {
			Collections.reverse(mediaList);
		}
		return mediaList;
	}

	public List<Media> sortByCreatedTime(List<Media> mediaList, boolean ascending) {

		Collections.sort(mediaList, this.createdDateComparator);

		if (ascending == false) {
			Collections.reverse(mediaList);
		}

		return mediaList;
	}

	private Comparator<Media> titleComparator = new Comparator<Media>() {
		public int compare(Media m1, Media m2) {
			return m1.getTitle().compareTo(m2.getTitle());
		}
	};

	private Comparator<Media> fileSizeComparator = new Comparator<Media>() {
		public int compare(Media m1, Media m2) {
			return Long.compare(m1.getFileSize(), m2.getFileSize());
		}
	};

	private Comparator<Media> createdDateComparator = new Comparator<Media>() {
		public int compare(Media m1, Media m2) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
			int result = 0;
			try {
				result = sdf.parse(m1.getCreatedTime()).compareTo(sdf.parse(m2.getCreatedTime()));
			} catch (ParseException e) {
				// TODO: Handle Exception
				e.printStackTrace();
			}
			return result;
		}
	};

}
