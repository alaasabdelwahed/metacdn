/**
 * This class is the business facade that receives the servlet requests and does the business logic
 */
package com.metacdn.business;

import java.io.IOException;
import java.util.List;

import com.metacdn.dataaccess.MediaAccessClient;
import com.metacdn.domain.Media;
import com.metacdn.domain.MediaContainer;
import com.metacdn.util.MediaMapper;
import com.metacdn.util.MediaSortingUtil;

/**
 * @author Alaa Abdelwahed
 *
 */
public class MediaManipulationFacade {

	private String sort;

	public MediaManipulationFacade(String sort) {
		this.sort = sort;
	}

	public String readMedias() throws IOException {

		MediaAccessClient mediaAccess = new MediaAccessClient();
		MediaMapper mediaMapper = new MediaMapper();
		MediaSortingUtil mediaSortingUtil = new MediaSortingUtil();

		MediaContainer mediaCon = null;
		//Retrieve JSON Data from API using the media access client
		String mediaConJson = mediaAccess.getJSONData();
		if (mediaConJson != null) {
			//convert the JSON data into a POJO
			mediaCon = mediaMapper.convertJSONtoObject(mediaConJson);
		}
		List<Media> mediaList = mediaCon.getMedias();

		if (sort != null) {
			//if a sorting criteria is define, perform sorting
			mediaList = mediaSortingUtil.sortList(mediaList, sort);
		}

		String medias = mediaMapper.convertObjectToJSON(mediaList);

		return medias;

	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

}
