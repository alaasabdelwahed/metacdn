package com.metacdn.services;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.metacdn.business.MediaManipulationFacade;

/**
 * @author Alaa Abdelwahed
 */
public class MediaServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final String SORT_PARAM = "sort";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		
		String sort = request.getParameter(SORT_PARAM);
		
		MediaManipulationFacade facade = new MediaManipulationFacade(sort);
		String medias = facade.readMedias();
		    
	    out.println(medias);
	    out.flush();
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
