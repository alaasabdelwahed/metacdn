/**
 * A POJO to map the JSON file into a java object 
 */
package com.metacdn.domain;

import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author Alaa Abdelwahed
 *
 */
@JsonPropertyOrder({
    "cursor",
    "size",
    "medias",
    "moreResult"
})
public class MediaContainer {
	 @JsonProperty("cursor")
	 private String cursor;
	 @JsonProperty("size")
	 private Integer size;
	 @JsonProperty("medias")
	 private List<Media> medias = new ArrayList<Media>();
	 @JsonProperty("moreResult")
	 private Boolean moreResult;
	 
	 @JsonProperty("cursor")
	 public String getCursor() {
		 return cursor;
	 }
	
	 @JsonProperty("cursor")
	 public void setCursor(String cursor) {
	    this.cursor = cursor;
	 }
	
	 @JsonProperty("size")
	 public Integer getSize() {
		 return size;
	 }
	
	 @JsonProperty("size")
	 public void setSize(Integer size) {
		 this.size = size;
	 }
	
	 @JsonProperty("medias")
	 public List<Media> getMedias() {
		 return medias;
	 }
	
	 @JsonProperty("medias")
	 public void setMedias(List<Media> medias) {
		 this.medias = medias;
	 }
	
	 @JsonProperty("moreResult")
	 public Boolean getMoreResult() {
		 return moreResult;
	 }
	
	 @JsonProperty("moreResult")
	 public void setMoreResult(Boolean moreResult) {
		 this.moreResult = moreResult;
	 }
}