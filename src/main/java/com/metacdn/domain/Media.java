/**
 * 
 */
package com.metacdn.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author Alaa Abdelwahed
 *
 */
@JsonPropertyOrder({
    "aspectRatio",
    "createdTime",
    "createdTimestamp",
    "description",
    "duplicates",
    "encodingStatus",
    "hostUntil",
    "mediaKey",
    "mediaName",
    "mediaUserString",
    "metaCdnUrl",
    "metaCdnHtml5Url",
    "originUrl",
    "screenshotUrl",
    "captionUrl",
    "splashScreenUrl",
    "title",
    "mediaStatus",
    "mediaBrandingTemplateName",
    "fileSize",
    "locations",
    "tags",
    "containers",
    "qualities",
    "profile",
    "gaId",
    "embedRestrictions",
    "adTagUrl"
})
public class Media {
	 	@JsonProperty("aspectRatio")
	    private String aspectRatio;
	    @JsonProperty("createdTime")
	    private String createdTime;
	    @JsonProperty("createdTimestamp")
	    private long createdTimestamp;
	    @JsonProperty("description")
	    private String description;
	    @JsonProperty("duplicates")
	    private Boolean duplicates;
	    @JsonProperty("encodingStatus")
	    private String encodingStatus;
	    @JsonProperty("hostUntil")
	    private Object hostUntil;
	    @JsonProperty("mediaKey")
	    private String mediaKey;
	    @JsonProperty("mediaName")
	    private String mediaName;
	    @JsonProperty("mediaUserString")
	    private String mediaUserString;
	    @JsonProperty("metaCdnUrl")
	    private String metaCdnUrl;
	    @JsonProperty("metaCdnHtml5Url")
	    private String metaCdnHtml5Url;
	    @JsonProperty("originUrl")
	    private String originUrl;
	    @JsonProperty("screenshotUrl")
	    private String screenshotUrl;
	    @JsonProperty("captionUrl")
	    private String captionUrl;
	    @JsonProperty("splashScreenUrl")
	    private String splashScreenUrl;
	    @JsonProperty("title")
	    private String title;
	    @JsonProperty("mediaStatus")
	    private String mediaStatus;
	    @JsonProperty("mediaBrandingTemplateName")
	    private String mediaBrandingTemplateName;
	    @JsonProperty("fileSize")
	    private long fileSize;
	    @JsonProperty("locations")
	    private List<String> locations = new ArrayList<String>();
	    @JsonProperty("tags")
	    private List<String> tags = new ArrayList<String>();
	    @JsonProperty("containers")
	    private List<String> containers = new ArrayList<String>();
	    @JsonProperty("qualities")
	    private List<String> qualities = new ArrayList<String>();
	    @JsonProperty("profile")
	    private String profile;
	    @JsonProperty("gaId")
	    private String gaId;
	    @JsonProperty("embedRestrictions")
	    private String embedRestrictions;
	    @JsonProperty("adTagUrl")
	    private Object adTagUrl;
	 
	    @JsonProperty("aspectRatio")
	    public String getAspectRatio() {
	        return aspectRatio;
	    }

	    @JsonProperty("aspectRatio")
	    public void setAspectRatio(String aspectRatio) {
	        this.aspectRatio = aspectRatio;
	    }

	    @JsonProperty("createdTime")
	    public String getCreatedTime() {
	        return createdTime;
	    }

	    @JsonProperty("createdTime")
	    public void setCreatedTime(String createdTime) {
	        this.createdTime = createdTime;
	    }

	    @JsonProperty("createdTimestamp")
	    public long getCreatedTimestamp() {
	        return createdTimestamp;
	    }

	    @JsonProperty("createdTimestamp")
	    public void setCreatedTimestamp(long createdTimestamp) {
	        this.createdTimestamp = createdTimestamp;
	    }

	    @JsonProperty("description")
	    public String getDescription() {
	        return description;
	    }

	    @JsonProperty("description")
	    public void setDescription(String description) {
	        this.description = description;
	    }

	    @JsonProperty("duplicates")
	    public Boolean getDuplicates() {
	        return duplicates;
	    }

	    @JsonProperty("duplicates")
	    public void setDuplicates(Boolean duplicates) {
	        this.duplicates = duplicates;
	    }

	    @JsonProperty("encodingStatus")
	    public String getEncodingStatus() {
	        return encodingStatus;
	    }

	    @JsonProperty("encodingStatus")
	    public void setEncodingStatus(String encodingStatus) {
	        this.encodingStatus = encodingStatus;
	    }

	    @JsonProperty("hostUntil")
	    public Object getHostUntil() {
	        return hostUntil;
	    }

	    @JsonProperty("hostUntil")
	    public void setHostUntil(Object hostUntil) {
	        this.hostUntil = hostUntil;
	    }

	    @JsonProperty("mediaKey")
	    public String getMediaKey() {
	        return mediaKey;
	    }

	    @JsonProperty("mediaKey")
	    public void setMediaKey(String mediaKey) {
	        this.mediaKey = mediaKey;
	    }

	    @JsonProperty("mediaName")
	    public String getMediaName() {
	        return mediaName;
	    }

	    @JsonProperty("mediaName")
	    public void setMediaName(String mediaName) {
	        this.mediaName = mediaName;
	    }

	    @JsonProperty("mediaUserString")
	    public String getMediaUserString() {
	        return mediaUserString;
	    }

	    @JsonProperty("mediaUserString")
	    public void setMediaUserString(String mediaUserString) {
	        this.mediaUserString = mediaUserString;
	    }

	    @JsonProperty("metaCdnUrl")
	    public String getMetaCdnUrl() {
	        return metaCdnUrl;
	    }

	    @JsonProperty("metaCdnUrl")
	    public void setMetaCdnUrl(String metaCdnUrl) {
	        this.metaCdnUrl = metaCdnUrl;
	    }

	    @JsonProperty("metaCdnHtml5Url")
	    public String getMetaCdnHtml5Url() {
	        return metaCdnHtml5Url;
	    }

	    @JsonProperty("metaCdnHtml5Url")
	    public void setMetaCdnHtml5Url(String metaCdnHtml5Url) {
	        this.metaCdnHtml5Url = metaCdnHtml5Url;
	    }

	    @JsonProperty("originUrl")
	    public String getOriginUrl() {
	        return originUrl;
	    }

	    @JsonProperty("originUrl")
	    public void setOriginUrl(String originUrl) {
	        this.originUrl = originUrl;
	    }

	    @JsonProperty("screenshotUrl")
	    public String getScreenshotUrl() {
	        return screenshotUrl;
	    }

	    @JsonProperty("screenshotUrl")
	    public void setScreenshotUrl(String screenshotUrl) {
	        this.screenshotUrl = screenshotUrl;
	    }

	    @JsonProperty("captionUrl")
	    public String getCaptionUrl() {
	        return captionUrl;
	    }

	    @JsonProperty("captionUrl")
	    public void setCaptionUrl(String captionUrl) {
	        this.captionUrl = captionUrl;
	    }

	    @JsonProperty("splashScreenUrl")
	    public String getSplashScreenUrl() {
	        return splashScreenUrl;
	    }

	    @JsonProperty("splashScreenUrl")
	    public void setSplashScreenUrl(String splashScreenUrl) {
	        this.splashScreenUrl = splashScreenUrl;
	    }

	    @JsonProperty("title")
	    public String getTitle() {
	        return title;
	    }

	    @JsonProperty("title")
	    public void setTitle(String title) {
	        this.title = title;
	    }

	    @JsonProperty("mediaStatus")
	    public String getMediaStatus() {
	        return mediaStatus;
	    }

	    @JsonProperty("mediaStatus")
	    public void setMediaStatus(String mediaStatus) {
	        this.mediaStatus = mediaStatus;
	    }

	    @JsonProperty("mediaBrandingTemplateName")
	    public String getMediaBrandingTemplateName() {
	        return mediaBrandingTemplateName;
	    }

	    @JsonProperty("mediaBrandingTemplateName")
	    public void setMediaBrandingTemplateName(String mediaBrandingTemplateName) {
	        this.mediaBrandingTemplateName = mediaBrandingTemplateName;
	    }

	    @JsonProperty("fileSize")
	    public long getFileSize() {
	        return fileSize;
	    }

	    @JsonProperty("fileSize")
	    public void setFileSize(long fileSize) {
	        this.fileSize = fileSize;
	    }

	    @JsonProperty("locations")
	    public List<String> getLocations() {
	        return locations;
	    }

	    @JsonProperty("locations")
	    public void setLocations(List<String> locations) {
	        this.locations = locations;
	    }

	    @JsonProperty("tags")
	    public List<String> getTags() {
	        return tags;
	    }

	    @JsonProperty("tags")
	    public void setTags(List<String> tags) {
	        this.tags = tags;
	    }

	    @JsonProperty("containers")
	    public List<String> getContainers() {
	        return containers;
	    }

	    @JsonProperty("containers")
	    public void setContainers(List<String> containers) {
	        this.containers = containers;
	    }

	    @JsonProperty("qualities")
	    public List<String> getQualities() {
	        return qualities;
	    }

	    @JsonProperty("qualities")
	    public void setQualities(List<String> qualities) {
	        this.qualities = qualities;
	    }

	    @JsonProperty("profile")
	    public String getProfile() {
	        return profile;
	    }

	    @JsonProperty("profile")
	    public void setProfile(String profile) {
	        this.profile = profile;
	    }

	    @JsonProperty("gaId")
	    public String getGaId() {
	        return gaId;
	    }

	    @JsonProperty("gaId")
	    public void setGaId(String gaId) {
	        this.gaId = gaId;
	    }

	    @JsonProperty("embedRestrictions")
	    public String getEmbedRestrictions() {
	        return embedRestrictions;
	    }

	    @JsonProperty("embedRestrictions")
	    public void setEmbedRestrictions(String embedRestrictions) {
	        this.embedRestrictions = embedRestrictions;
	    }

	    @JsonProperty("adTagUrl")
	    public Object getAdTagUrl() {
	        return adTagUrl;
	    }

	    @JsonProperty("adTagUrl")
	    public void setAdTagUrl(Object adTagUrl) {
	        this.adTagUrl = adTagUrl;
	    }
}
