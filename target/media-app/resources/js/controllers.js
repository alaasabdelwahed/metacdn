'use strict';

angular.module('mediaApp')
	.controller('ViewMediaController', ['$scope', 'getMediaService','sortMediaService', 
	                                    function($scope, getMediaService, sortMediaService){
		
		$scope.medias = [];
		var ascendingTitle = true;	
		var ascendingTime = true;
		var ascendingSize = true;
		
		getMediaService.getMedias()
			.then(
			  //in case of success
			  function(response){
	             $scope.medias = response.data;
	          },
	          //in case of failure
	          function(response){
	             $scope.message="Error: " + response.status + " " + response.statusText;
	          }
			);
		
		$scope.sortByTitle = function(){
			if(ascendingTitle){
				sortMediaService.sortByTitle(ascendingTitle)
				.then(
				  //in case of success
				  function(response){
		             $scope.medias = response.data;
		             ascendingTitle = false;
		          },
		          //in case of failure
		          function(response){
		             $scope.message="Error: " + response.status + " " + response.statusText;
		          }
			)}
			else{
				sortMediaService.sortByTitle(ascendingTitle)
				.then(
				  //in case of success
				  function(response){
		             $scope.medias = response.data;
		             ascendingTitle = true;
		          },
		          //in case of failure
		          function(response){
		             $scope.message="Error: " + response.status + " " + response.statusText;
		          }
			)};
		};
		
		$scope.sortByFileSize = function(){
			if(ascendingSize){
				sortMediaService.sortByFileSize(ascendingSize)
				.then(
				  //in case of success
				  function(response){
		             $scope.medias = response.data;
		             ascendingSize = false;
		          },
		          //in case of failure
		          function(response){
		             $scope.message="Error: " + response.status + " " + response.statusText;
		          }
			)}
			else{
				sortMediaService.sortByFileSize(ascendingSize)
				.then(
				  //in case of success
				  function(response){
		             $scope.medias = response.data;
		             ascendingSize = true;
		          },
		          //in case of failure
		          function(response){
		             $scope.message="Error: " + response.status + " " + response.statusText;
		          }
			)};
		};

		$scope.sortByCreatedTime = function(){
			if(ascendingTime){
				sortMediaService.sortByCreatedTime(ascendingTime)
				.then(
				  //in case of success
				  function(response){
		             $scope.medias = response.data;
		             ascendingTime = false;
		          },
		          //in case of failure
		          function(response){
		             $scope.message="Error: " + response.status + " " + response.statusText;
		          }
			)}
			else{
				sortMediaService.sortByCreatedTime(ascendingTime)
				.then(
				  //in case of success
				  function(response){
		             $scope.medias = response.data;
		             ascendingTime = true;
		          },
		          //in case of failure
		          function(response){
		             $scope.message="Error: " + response.status + " " + response.statusText;
		          }
			)};
		};
		
	}]);