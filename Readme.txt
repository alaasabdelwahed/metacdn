This application is concerned with reading media information from an API in the form of JSON.
It then views this data in an HTML page, and provides sorting mechanisms for this data.

Technologies used:
Java 7, Maven, Bower, AngularJS, Bootstrap, JSON

Description:
The project is divided into 3 layers:
1- Presentation layer: contains the HTML, Javascript & CSS files, as well as the AngularJS & Bootstrap Libraries.
2- Business layer:
	- MediaServlet: receives requests from the front-end and sends the respond back
	- MediaManipulationFacade: receives the request from the Servlet and decides the appropriate functionality
	- MediaMapper: converts the JSON file into a java object and vice versa
	- MediaSortingUtil: performs sorting functionality over the acquired data
3- Data Layer:
	- MediaAccessClient: responsible for retrieving the JSON data from the API
	- Media & MediaContatiner: POJO files that are used for mapping the JSON data into Java objects

Configuration files:
1- media-app/src/main/resources/config.properties: contains the URL and credentials used to access the API
2- media-app/src/main/webapp/WEB-INF/web.xml: contains the servlet mapping information
3- media-app/pom.xml: contains the maven dependencies that are used in the project:
	- JUnit
	- Jackson mapper: used for mapping JSON to POJO's and vice versa
	- Javax.servlet: servlets library


How to run the application:
1- Deploy media-app.war on a web server. Tomcat was used for this project.
2- Start the server
3- Type the following URL into a browser: http://localhost:8081/media-app/index.html


Note:
The sorting button in the HTML page is not very pretty! 
It was supposed to be a link but I failed to make it connect to the servlet properly.
Also the toggling mechanism is not the best. It is done by hand! 
I'm working on my front end skills so this is the best I could come up with so far.
I hope I can improve the front end a bit more if I have time.